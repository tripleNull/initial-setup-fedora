#!/bin/bash

# Fedora setup script - setup initially with Fedora 28, likely will apply to later versions

# Update repos and install common apps from the repos
sudo dnf upgrade

# Sublime Text
install_subl(){
	# Add repo
	sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
	sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo

	# Install
	sudo dnf install sublime-text
}

# Ask the user if they want to install sublime-text
read -p 'Install Sublime Text? (Y/n): ' subyn

if [[ $subyn = "" ]]
then
	subyn=Y
fi

case $subyn in
	[yY])
		install_subl
	;;
	*)
		echo "Skipping..."
	;;
esac

# Install common apps
sudo dnf install keepassxc filezilla neofetch

# add in a bin directory under home, if it does not already exist
if [[ -d $HOME/bin ]] 
then
	echo "~~~~~bin already exists under home"
else
	echo "~~~~~Creating bin directory under home"
	mkdir $HOME/bin
fi

# start generating a key for ssh
ssh-keygen -b 4096

# Ask the user to reboot, needed if replacing the open source drivers or if a kernel update is downloaded
read -p 'Would you like to reboot to commit changes? (Y/n): ' rebyn

if [[ $rebyn = "" ]]
then
	echo "Defaulting to Y"
	rebyn=Y
fi

case $rebyn in
	[yY])
		echo "Rebooting" && sudo reboot
	;;
	*)
		echo "Please reboot when convenient to commit changes"
	;;
esac
